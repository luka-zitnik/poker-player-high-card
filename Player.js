class Player {
  static get VERSION() {
    return '0.1';
  }

  static betRequest(gameState) {
    var activePlayers = gameState.players.filter(function (player) {
      return player.status === 'active';
    });
    var investedPlayers = gameState.players.filter(function (player) {
      return player.stack < 500;
    });
    var current_buy_in = gameState.current_buy_in;
    var minimum_raise = gameState.minimum_raise;
    var me = gameState.players[gameState.in_action];
    var allCards = me.hole_cards.concat(gameState.community_cards);

    if (activePlayers.length === 2) {
      return current_buy_in - me.bet + me.stack;
    }

    // Good cards, so bet:

    if (me.hole_cards[0].rank == me.hole_cards[1].rank && me.hole_cards[0].rank > 9) {

        if (activePlayers > 2) {
          return current_buy_in - me.bet + minimum_raise;
        }

        return current_buy_in - me.bet + me.stack;
    }

    // Bad situation, back away

    if (investedPlayers.length > 1 && investedPlayers.indexOf(me) === -1) {
      return 0;
    }

    // Just call otherwise

    return current_buy_in - me.bet;
  }

  static showdown(gameState) {
    console.log(gameState);
  }
}

module.exports = Player;
